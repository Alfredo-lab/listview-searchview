package com.example.listview_personalizado;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;


public class ListViewAdapter extends ArrayAdapter<ItemData> {
    int groupId;
    Activity Context;
    ArrayList<ItemData> list;
    private ArrayList<ItemData> listFilter;
    LayoutInflater inflater;

    public ListViewAdapter(Activity Context,int groupId,int id,ArrayList<ItemData> list){
        super(Context,id,list);
        this.list = list;
        this.listFilter = list;
        inflater = (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
    }

    public View getView(int posicion, View convertView, ViewGroup parent){
        View itemView = inflater.inflate(groupId,parent,false);
        ImageView imagen = (ImageView) itemView.findViewById(R.id.image);
        imagen.setImageResource(list.get(posicion).getImageId());
        TextView textCategoria = (TextView) itemView.findViewById(R.id.textView1);
        textCategoria.setText(list.get(posicion).getTextCategoria());
        TextView textDescripcion = (TextView) itemView.findViewById(R.id.textView2);
        textDescripcion.setText(list.get(posicion).getTextDescripcion());
        return itemView;
    }

    public View getDropDownView(int posicion,View convertView,ViewGroup parent){
        return getView(posicion,convertView,parent);
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public ItemData getItem(int pos)
    {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos)
    {
        return list.indexOf(getItem(pos));
    }

    @Override
    public Filter getFilter()
    {
        return new ListFilter();
    }

    private class ListFilter extends Filter{

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if(constraint != null && constraint.length() > 0)
            {
                constraint = constraint.toString().toUpperCase();
                ArrayList<ItemData> filter = new ArrayList<>();
                for(int x = 0; x < listFilter.size(); x++)
                {
                    if(listFilter.get(x).getTextCategoria().toUpperCase().contains(constraint))
                    {
                        ItemData itemData = listFilter.get(x);
                        filter.add(itemData);
                    }
                    results.count = filter.size();
                    results.values = filter;
                }

            }
            else
            {
                results.count = listFilter.size();
                results.values = listFilter;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list = (ArrayList<ItemData>) results.values;
            notifyDataSetChanged();
        }
    }

}

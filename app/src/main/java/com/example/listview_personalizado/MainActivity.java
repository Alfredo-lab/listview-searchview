package com.example.listview_personalizado;

import androidx.appcompat.app.AppCompatActivity;

import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;


import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(getString(R.string.itemFacebook),getString(R.string.msgFacebook),R.drawable.purepng));
        list.add(new ItemData(getString(R.string.itemTwiter),getString(R.string.msgTwiter),R.drawable.karate));
        list.add(new ItemData(getString(R.string.itemInstagram),getString(R.string.msgInstagram),R.drawable.fut));
        list.add(new ItemData(getString(R.string.itemWhatsap),getString(R.string.msgWhatsap),R.drawable.musica));
        list.add(new ItemData(getString(R.string.itemSnapchat),getString(R.string.msgSnapchat),R.drawable.baile));

        listView = (ListView) findViewById(R.id.ListView);
        SearchView srcLista = (SearchView) findViewById(R.id.menu_search);


         adapter = new ListViewAdapter(this,R.layout.row,R.id.textView1,list);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(),getString(R.string.msgSeleccionado).toString() +" "+((ItemData) parent.getItemAtPosition(position)).getTextCategoria(),
                        Toast.LENGTH_SHORT).show();
            }
        });


    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.search_view ,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);

                return false;
            }
        });
        return super.onCreateOptionsMenu(menu); }


}
